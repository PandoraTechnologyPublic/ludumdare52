using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora
{
    public class FieldComponent : MonoBehaviour
    {
        private static readonly float USAGE_TIME_REQUIRED = 4.0f;

        [SerializeField] private List<SpriteRenderer> _fieldStates;
        [SerializeField] private Canvas _fieldProgressBarCanvas;
        [SerializeField] private Image _fieldProgressBar;

        private float _timeUsed;
        private int _fieldStateIndex;

        private GameManager _gameManager;

        private void Start()
        {
            _gameManager = GameObject.FindObjectOfType<GameManager>();

            foreach (SpriteRenderer spriteRenderer in _fieldStates)
            {
                spriteRenderer.gameObject.SetActive(false);
            }

            _fieldStates[_fieldStateIndex].gameObject.SetActive(true);
            _fieldProgressBarCanvas.gameObject.SetActive(false);

        }

        public void Use(PlayerController player)
        {
            _timeUsed += Time.deltaTime * player.GetFarmSpeed();

            _fieldProgressBarCanvas.gameObject.SetActive(true);
            _fieldProgressBar.fillAmount = _timeUsed / USAGE_TIME_REQUIRED;

            if (_timeUsed >= USAGE_TIME_REQUIRED)
            {
                _timeUsed = 0;

                _fieldStates[_fieldStateIndex].gameObject.SetActive(false);

                // :TODO: CHANGE FIELD STATE
                _fieldStateIndex = (_fieldStateIndex + 1) % _fieldStates.Count;

                _fieldStates[_fieldStateIndex].gameObject.SetActive(true);

                if (_fieldStateIndex == 0)
                {
                    _gameManager.AddFarmedField(this);
                    // :TODO: generate boss ?
                    // :TODO: disappears
                }
            }
        }
    }
}