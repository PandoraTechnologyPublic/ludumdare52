using TMPro;
using UnityEngine;

namespace Pandora.Ui
{
    public class PerkUiButton : MonoBehaviour
    {
        [SerializeField] private TMP_Text _perkName;
        [SerializeField] private TMP_Text _perkLevel;
        [SerializeField] private TMP_Text _perkDescription;

        public void SetData(PlayerController player, PerkComponent perkComponent)
        {
            _perkName.SetText(perkComponent.GetPerkName(player));
            _perkLevel.SetText(perkComponent.GetPerkLevelText(player));
            _perkDescription.SetText(perkComponent.GetPerkDescription(player));

            if (perkComponent.IsMaxed(player))
            {
                _perkLevel.SetText("Maxed!");
                _perkDescription.gameObject.SetActive(false);
            }
        }
    }
}