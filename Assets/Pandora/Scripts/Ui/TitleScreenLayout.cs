using System;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Pandora
{
    public class TitleScreenLayout : MonoBehaviour
    {
        [SerializeField] private List<Animator> _animators;
        [SerializeField] private TMP_Text _pressStartText;
        [SerializeField] private Image _fadeScreen;
        [SerializeField] private AudioSource _titleScreenMusic;

        private Inputs _gameInputs;
        private bool _isFading;

        private void Start()
        {
            foreach (Animator animator in _animators)
            {
                animator.SetTrigger("moving");
            }

            MakeTextBlink();

            _gameInputs = new Inputs();
            _gameInputs.Enable();
        }

        private void Update()
        {
            if (!_isFading && (_gameInputs.UI.Continue.WasReleasedThisFrame() || _gameInputs.UI.Restart.WasReleasedThisFrame()))
            {
                DOTween.Sequence()
                    .Append(_fadeScreen.DOFade(1, 0.6f))
                    .Append(_titleScreenMusic.DOFade(0, 0.6f))
                    .AppendCallback(() => SceneManager.LoadScene(1));

                _gameInputs.Disable();
            }
        }

        private void MakeTextBlink()
        {
            DOTween.Sequence()
                .Append(_pressStartText.DOFade(0, 0.5f))
                .Append(_pressStartText.DOFade(1, 0.5f))
                .AppendCallback(MakeTextBlink);
        }
    }
}