using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora.Ui
{
    public class LevelUpUiScreen : MonoBehaviour
    {
        private static readonly int PERK_SELECTION_COUNT = 4;

        [SerializeField] private AudioSource _perkSelectedSound;
        [SerializeField] private PerkUiButton _perkUiButtonPrototype;
        [SerializeField] private RectTransform _perkButtonPanel;

        private PlayerController _player;
        private readonly List<PerkUiButton> _perkUiButtons = new List<PerkUiButton>();

        private GameManager _gameManager;

        private void Awake()
        {
            _gameManager = GameObject.FindObjectOfType<GameManager>();

            gameObject.SetActive(false);
        }

        private void Start()
        {
            _perkUiButtonPrototype.gameObject.SetActive(false);
            _perkUiButtons.Add(_perkUiButtonPrototype);
        }

        private void OnButtonClick(PlayerController player, PerkComponent perkComponent)
        {
            perkComponent.LevelUp(player);
            _perkSelectedSound.Play(); //:TODO: it is not heard because de parent is closed too soon
            gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            _gameManager.PauseGame();

            _player = GameObject.FindObjectOfType<PlayerController>();

            List<PerkComponent> availablePerks = _player.GetAvailablePerks();
            List<PerkComponent> selectedPerks = new List<PerkComponent>();
            int perkSelectionCount = Mathf.Min(PERK_SELECTION_COUNT, availablePerks.Count);

            while (selectedPerks.Count < perkSelectionCount)
            {
                PerkComponent selectedPerk = availablePerks[Random.Range(0, availablePerks.Count)];

                if (!selectedPerks.Contains(selectedPerk))
                {
                    selectedPerks.Add(selectedPerk);
                }
            }

            int perkIndex = 0;

            for (; perkIndex < selectedPerks.Count; perkIndex++)
            {
                PerkComponent perkComponent = selectedPerks[perkIndex];

                if (perkIndex >= _perkUiButtons.Count)
                {
                    PerkUiButton perkUiButton = GameObject.Instantiate(_perkUiButtonPrototype, _perkButtonPanel);

                    _perkUiButtons.Add(perkUiButton);
                }

                PerkUiButton uiButton = _perkUiButtons[perkIndex];

                uiButton.SetData(_player, perkComponent);
                uiButton.gameObject.SetActive(true);

                Button button = uiButton.GetComponent<Button>();

                button.onClick.RemoveAllListeners();
                button.onClick.AddListener(() => OnButtonClick(_player, perkComponent));
                button.interactable = !perkComponent.IsMaxed(_player);
                if (perkIndex == 0)
                {
                    button.Select();
                }
            }

            for (; perkIndex < _perkUiButtons.Count; perkIndex++)
            {
                _perkUiButtons[perkIndex].gameObject.SetActive(false);
            }
        }

        private void OnDisable()
        {
            _gameManager.UnPauseGame();
        }
    }
}