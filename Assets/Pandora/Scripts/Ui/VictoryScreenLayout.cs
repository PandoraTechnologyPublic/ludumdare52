using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pandora.Ui
{
    public class VictoryScreenLayout : MonoBehaviour
    {
        private InputManager _inputManager;
        private GameManager _gameManager;

        private void Awake()
        {
            _gameManager = GameObject.FindObjectOfType<GameManager>();
        }

        private void Start()
        {
            _inputManager = GameObject.FindObjectOfType<InputManager>();
        }

        private void OnEnable()
        {
            _gameManager.PauseGame();
        }

        private void OnDisable()
        {
            _gameManager.UnPauseGame();
        }

        private void Update()
        {
            if (_inputManager.GetGameInputs().UI.Continue.WasReleasedThisFrame())
            {
                gameObject.SetActive(false);
            }

            if (_inputManager.GetGameInputs().UI.Restart.WasReleasedThisFrame())
            {
                SceneManager.LoadScene(0);
            }
        }
    }
}