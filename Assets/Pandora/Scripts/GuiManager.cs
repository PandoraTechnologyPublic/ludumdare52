using System;
using System.Collections;
using Pandora.Ui;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Pandora
{
    public class GuiManager : MonoBehaviour
    {
        [SerializeField] private Image _playerXpBar;
        [SerializeField] private Image _playerLifeBar;
        [SerializeField] private TMP_Text _timeText;
        [SerializeField] private TMP_Text _farmedFieldText;
        [SerializeField] private TMP_Text _hitPointPrototype;
        [SerializeField] private LevelUpUiScreen _levelUpUiScreen;
        [SerializeField] private RectTransform _gameOverPanel;
        [SerializeField] private VictoryScreenLayout _victoryScreen;

        private Camera _camera;
        private GameManager _gameManager;

        private void Start()
        {
            _gameManager = GameObject.FindObjectOfType<GameManager>();
            _camera = Camera.main;

            _levelUpUiScreen.gameObject.SetActive(false);
            _gameOverPanel.gameObject.SetActive(false);
            _victoryScreen.gameObject.SetActive(false);
            _hitPointPrototype.gameObject.SetActive(false);
        }

        private void Update()
        {
            float elapsedTime = _gameManager.GetElapsedTime();

            _timeText.SetText($"{Mathf.FloorToInt(elapsedTime / 60):00}:{Mathf.FloorToInt(elapsedTime % 60):00}");
        }

        public void ShowGameOverScreen()
        {
            _gameOverPanel.gameObject.SetActive(true);
            _gameManager.PauseGame();
        }

        public void ShowVictoryScreen()
        {
            _victoryScreen.gameObject.SetActive(true);
        }

        public void ShowLevelUpScreen()
        {
            _levelUpUiScreen.gameObject.SetActive(true);
        }

        public void AnimateHit(Vector3 worldPosition, float damageCount)
        {
            TMP_Text hitPointEffect = GameObject.Instantiate(_hitPointPrototype, _hitPointPrototype.transform.parent);

            hitPointEffect.SetText($"-{damageCount}");
            hitPointEffect.gameObject.SetActive(true);

            StartCoroutine(AnimateText(hitPointEffect, _camera.WorldToScreenPoint(worldPosition)));
        }

        private IEnumerator AnimateText(TMP_Text hitTextEffect, Vector3 screenPoint)
        {
            hitTextEffect.rectTransform.position = screenPoint;

            Vector3 currentPosition = screenPoint;
            float spentTime = 0;
            int xRange = Random.Range(-100, 100);

            while (spentTime < 3.0f)
            {
                spentTime += Time.deltaTime;

                yield return null;

                float time = spentTime - 3.0f/2;
                currentPosition.y += Time.deltaTime * 100.0f * -time;//Mathf.Sign(time);
                currentPosition.x += Time.deltaTime * xRange;

                hitTextEffect.rectTransform.position = currentPosition;
            }

            GameObject.Destroy(hitTextEffect.gameObject);
        }

        public void SetPlayerLifePoints(float lifePoints, float maxLifePoints)
        {
            _playerLifeBar.fillAmount = lifePoints / maxLifePoints;
        }

        public void SetPlayerXpPoints(int dropCount, float currentXpNeededToLevelUpCount)
        {
            _playerXpBar.fillAmount = dropCount / currentXpNeededToLevelUpCount;
        }

        public void SetFieldFarmed(int farmedFieldCount, float fieldToFarmCount)
        {
            _farmedFieldText.SetText($"Fields farmed: {farmedFieldCount}/{fieldToFarmCount}");
        }
    }
}