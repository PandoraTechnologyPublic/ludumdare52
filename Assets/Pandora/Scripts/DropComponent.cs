using System;
using UnityEngine;

namespace Pandora
{
    public class DropComponent : MonoBehaviour
    {
        private static readonly float SEED_PICK_UP_COOLDOWN = 0.5f;

        private float _pickUpCooldown;

        private void Start()
        {
            _pickUpCooldown = SEED_PICK_UP_COOLDOWN;
        }

        private void Update()
        {
            _pickUpCooldown -= Time.deltaTime;
        }

        public bool IsPickable()
        {
            return _pickUpCooldown <= 0;
        }
    }
}