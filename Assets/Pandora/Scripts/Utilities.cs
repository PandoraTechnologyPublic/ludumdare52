using System.Collections.Generic;
using UnityEngine;

namespace Pandora
{
    public static class Utilities
    {
        public static EnemyController GetClosestEnemy(EnemyController[] enemyControllers, Vector3 playerPosition)
        {
            float closestEnemyDistance = float.MaxValue;
            EnemyController closestEnemy = null;

            foreach (EnemyController enemyController in enemyControllers)
            {
                float enemyDistance = (playerPosition - enemyController.transform.position).magnitude;

                if (enemyDistance < closestEnemyDistance)
                {
                    closestEnemyDistance = enemyDistance;
                    closestEnemy = enemyController;
                }
            }

            return closestEnemy;
        }

        public static List<EnemyController> GetClosestEnemies(EnemyController[] enemyControllers, Vector3 worldPosition)
        {
            List<EnemyController> sortedEnemies = new List<EnemyController>(enemyControllers);

            sortedEnemies.Sort((firstEnemy, secondEnemy) =>
            {
                float firstEnemyDistance = (worldPosition - firstEnemy.transform.position).magnitude;
                float secondEnemyDistance = (worldPosition - secondEnemy.transform.position).magnitude;

                return Mathf.RoundToInt(firstEnemyDistance - secondEnemyDistance);
            });

            return sortedEnemies;
        }

        public static List<EnemyController> GetEnemiesInRange(EnemyController[] enemyControllers, Vector3 playerPosition, float range)
        {
            List<EnemyController> enemiesInRange = new List<EnemyController>();

            foreach (EnemyController enemyController in enemyControllers)
            {
                float enemyDistance = (playerPosition - enemyController.transform.position).magnitude;

                if (enemyDistance < range)
                {
                    enemiesInRange.Add(enemyController);
                }
            }

            return enemiesInRange;
        }

        public static FieldComponent GetClosestField(FieldComponent[] fields, Vector3 playerPosition)
        {
            float closestFieldDistance = float.MaxValue;
            FieldComponent closestField = null;

            foreach (FieldComponent field in fields)
            {
                float fieldDistance = (playerPosition - field.transform.position).magnitude;

                if (fieldDistance < closestFieldDistance)
                {
                    closestFieldDistance = fieldDistance;
                    closestField = field;
                }
            }

            return closestField;
        }
    }
}