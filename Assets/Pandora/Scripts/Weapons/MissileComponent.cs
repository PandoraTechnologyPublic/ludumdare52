using System;
using DG.Tweening;
using UnityEngine;

namespace Pandora
{
    public class MissileComponent : MonoBehaviour
    {
        [SerializeField] private Animator _explosionAnimation;
        [SerializeField] private AudioSource _explosionSound;
        [SerializeField] private SpriteRenderer _sprite;

        private static readonly float MISSILE_SPEED = 8.0f;
        private static readonly float MISSILE_LIFETIME = 2.0f;
        private static readonly float MISSILE_RANGE = 2.0f;

        private float _currentMissileLifetime;
        private EnemyController _target;
        private int _missileDamage;
        private bool _isExploding;

        public void SetTarget(EnemyController enemyController)
        {
            _target = enemyController;

            _currentMissileLifetime = MISSILE_LIFETIME;

            _explosionAnimation.gameObject.SetActive(false);
        }

        private void Update()
        {
            if (!_isExploding)
            {
                _currentMissileLifetime -= Time.deltaTime;

                if ((_target == null) || (_currentMissileLifetime <= 0))
                {
                    Explode();
                }
                else
                {
                    Vector3 missilePosition = transform.position;
                    missilePosition.y = 0;

                    if (Vector3.Distance(_target.transform.position, missilePosition) <= MISSILE_RANGE)
                    {
                        Explode();

                        _target.Hit(_missileDamage);
                    }
                    else
                    {
                        // :TODO: Handle rotation
                        // missileDirection.Normalize();
                        // missileDirection *= (INITIAL_MISSILE_SPEED * Time.deltaTime);

                        // missileDirection.y = 0;

                        transform.Translate(Vector3.forward * MISSILE_SPEED * Time.deltaTime);
                    }
                }
            }
        }

        private void Explode()
        {
            _explosionSound.Play();
            transform.rotation = Quaternion.identity;
            _explosionAnimation.gameObject.SetActive(true);
            _sprite.gameObject.SetActive(false);
            _explosionAnimation.SetTrigger("play");

            _isExploding = true;

            DOTween.Sequence()
                .AppendInterval(0.5f)
                .AppendCallback(() => GameObject.Destroy(gameObject));
        }

        public void SetMissileDamage(int missileDamage)
        {
            _missileDamage = missileDamage;
        }
    }
}