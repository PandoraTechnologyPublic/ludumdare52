using System.Collections.Generic;
using UnityEngine;

namespace Pandora
{
    public class WeaponHomingMissileLauncher : MonoBehaviour
    {
        private static readonly int INITIAL_MISSILE_COUNT = 1;

        [SerializeField] private MissileComponent _missilePrototype;
        [SerializeField] private AudioSource _attackSound;

        private float _attackCooldowm;

        private float _currentAttackCooldown;

        private PlayerController _player;
        private int _currentMissileDamage;

        private void Start()
        {
            _currentAttackCooldown = _attackCooldowm;

            _missilePrototype.gameObject.SetActive(false);
            
            _player = GetComponentInParent<PlayerController>();
        }

        private void Update()
        {
            if (_player.GetIsAlive())
            {
                if (_currentAttackCooldown > 0)
                {
                    _currentAttackCooldown = Mathf.Max(0, _currentAttackCooldown - Time.deltaTime);
                }

                if (_currentAttackCooldown <= 0)
                {
                    _currentAttackCooldown = _attackCooldowm;

                    EnemyController[] enemyControllers = GameObject.FindObjectsOfType<EnemyController>();
                    List<EnemyController> enemiesInRange = Utilities.GetClosestEnemies(enemyControllers, transform.position);

                    for (int enemyIndex = 0; enemyIndex < enemiesInRange.Count; enemyIndex++)
                    {
                        if (enemyIndex < INITIAL_MISSILE_COUNT)
                        {
                            EnemyController enemyController = enemiesInRange[enemyIndex];
                            Vector3 weaponPosition = transform.position;
                            Vector3 enemyDirection = enemyController.transform.position - weaponPosition;

                            enemyDirection.y = 0;

                            MissileComponent missile = GameObject.Instantiate(_missilePrototype, weaponPosition + Vector3.up, Quaternion.LookRotation(enemyDirection));

                            _attackSound.Play();

                            missile.gameObject.SetActive(true);
                            missile.SetTarget(enemyController);
                            missile.SetMissileDamage(_currentMissileDamage);

                        }
                    }
                }
            }
        }

        public void SetAttackCooldown(float attackCooldown)
        {
            _attackCooldowm = attackCooldown;
        }

        public void SetAttackDamage(int damage)
        {
            _currentMissileDamage = damage;
        }
    }
}