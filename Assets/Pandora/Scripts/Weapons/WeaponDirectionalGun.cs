using UnityEngine;

namespace Pandora
{
    public class WeaponDirectionalGun : MonoBehaviour
    {
        [SerializeField] private BulletComponent _bulletPrototype;
        [SerializeField] private AudioSource _attackSound;

        private float _attackCooldowm;
        private float _currentAttackCooldown;

        private PlayerController _player;
        private int _attackDamage;

        private void Start()
        {
            _currentAttackCooldown = _attackCooldowm;

            _bulletPrototype.gameObject.SetActive(false);
            
            _player = GetComponentInParent<PlayerController>();
        }

        private void Update()
        {
            if (_player.GetIsAlive())
            {
                if (_currentAttackCooldown > 0)
                {
                    _currentAttackCooldown = Mathf.Max(0, _currentAttackCooldown - Time.deltaTime);
                }

                if (_currentAttackCooldown <= 0)
                {
                    _currentAttackCooldown = _attackCooldowm;

                    Vector3 playerDirection = _player.GetLastDirection();

                    Vector3 weaponPosition = transform.position;

                    BulletComponent bullet = GameObject.Instantiate(_bulletPrototype, weaponPosition + Vector3.up, Quaternion.LookRotation(playerDirection));

                    bullet.SetDamage(_attackDamage);
                    _attackSound.Play();

                    bullet.gameObject.SetActive(true);
                }
            }
        }

        public void SetAttackCooldown(float attackCooldown)
        {
            _attackCooldowm = attackCooldown;
        }

        public void SetAttackDamage(int attackDamage)
        {
            _attackDamage = attackDamage;
        }
    }
}