using DG.Tweening;
using UnityEngine;

namespace Pandora
{
    public class WeaponDirectionalMelee : MonoBehaviour
    {
        private static readonly float WEAPON_REACH = 2.5f;
        private static readonly float WEAPON_ATTACK_RANGE = 2.5f;

        [SerializeField] private SpriteRenderer _attackFx;
        [SerializeField] private AudioSource _attackSound;

        private float _attackCooldowm;
        private float _currentAttackCooldown;

        private PlayerController _player;
        private int _attackDamage;
        private bool _isAttackingBehind;

        private void Start()
        {
            _currentAttackCooldown = _attackCooldowm;

            _player = GetComponentInParent<PlayerController>();

            _attackFx.gameObject.SetActive(false);
        }

        private void Update()
        {
            if (_player.GetIsAlive())
            {
                if (_currentAttackCooldown > 0)
                {
                    _currentAttackCooldown = Mathf.Max(0, _currentAttackCooldown - Time.deltaTime);
                }

                if (_currentAttackCooldown <= 0)
                {
                    _currentAttackCooldown = _attackCooldowm;

                    Vector3 playerDirection = _player.GetLastDirection();

                    if (_player.GetIsLeftOriented())
                    {
                        playerDirection = Vector3.left;
                    }
                    else
                    {
                        playerDirection = -Vector3.left;
                    }

                    Sequence attackSequence = DOTween.Sequence()
                        .AppendCallback(() => AttackDirection(playerDirection))
                        .AppendInterval(0.3f)
                        .AppendCallback(() => _attackFx.gameObject.SetActive(false));

                    if (_isAttackingBehind)
                    {
                        attackSequence
                            .AppendCallback(() => AttackDirection(-playerDirection))
                            .AppendInterval(0.3f)
                            .AppendCallback(() => _attackFx.gameObject.SetActive(false));
                    }

                    attackSequence
                        .AppendCallback(() => _currentAttackCooldown = _attackCooldowm);
                }
            }
        }

        private void AttackDirection(Vector3 attackDirection)
        {
            transform.rotation = Quaternion.LookRotation(attackDirection);

            Vector3 attackPosition = transform.position + transform.TransformDirection(Vector3.forward) * WEAPON_REACH;

            // Debug.DrawLine(attackPosition, attackPosition + Vector3.up, Color.red, 1);
            //
            // Debug.DrawLine(attackPosition + Vector3.forward * WEAPON_ATTACK_RANGE, attackPosition + Vector3.up + Vector3.forward * WEAPON_ATTACK_RANGE,  Color.blue, 1);
            // Debug.DrawLine(attackPosition - Vector3.forward * WEAPON_ATTACK_RANGE, attackPosition + Vector3.up - Vector3.forward * WEAPON_ATTACK_RANGE,  Color.blue, 1);
            // Debug.DrawLine(attackPosition + Vector3.right * WEAPON_ATTACK_RANGE, attackPosition + Vector3.up + Vector3.right * WEAPON_ATTACK_RANGE,  Color.blue, 1);
            // Debug.DrawLine(attackPosition - Vector3.right * WEAPON_ATTACK_RANGE, attackPosition + Vector3.up - Vector3.right * WEAPON_ATTACK_RANGE,  Color.blue, 1);

            EnemyController[] enemyControllers = GameObject.FindObjectsOfType<EnemyController>();

            for (int enemyIndex = enemyControllers.Length - 1; enemyIndex >= 0; enemyIndex--)
            {
                EnemyController enemyController = enemyControllers[enemyIndex];

                if (Vector3.Distance(enemyController.transform.position, attackPosition) < WEAPON_ATTACK_RANGE)
                {
                    enemyController.Hit(_attackDamage);
                }
            }

            _attackFx.gameObject.SetActive(true);
            _attackSound.Play();
        }

        public void SetAttackCooldown(float attackCooldown)
        {
            _attackCooldowm = attackCooldown;
        }

        public void SetAttackDamage(int attackDamage)
        {
            _attackDamage = attackDamage;
        }

        public void SetAttackingBehind()
        {
            _isAttackingBehind = true;
        }
    }
}