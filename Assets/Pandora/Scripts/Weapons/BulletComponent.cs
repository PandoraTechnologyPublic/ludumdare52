using System.Collections.Generic;
using UnityEngine;

namespace Pandora
{
    public class BulletComponent : MonoBehaviour
    {
        private static readonly float BULLET_SPEED = 12.0f;
        private static readonly float BULLET_LIFETIME = 2.0f;
        private static readonly float BULLET_RANGE = 2.0f;

        private float _currentBulletLifetime;
        private int _currentDamage;

        private void Start()
        {
            _currentBulletLifetime = BULLET_LIFETIME;
        }

        private void Update()
        {
            _currentBulletLifetime -= Time.deltaTime;

            if (_currentBulletLifetime <= 0)
            {
                GameObject.Destroy(gameObject);
            }
            else
            {
                Vector3 bulletPosition = transform.position;
                bulletPosition.y = 0;

                EnemyController[] enemyControllers = GameObject.FindObjectsOfType<EnemyController>();
                List<EnemyController> closestEnemies = Utilities.GetClosestEnemies(enemyControllers, bulletPosition);

                if ((closestEnemies.Count > 0) && (Vector3.Distance(closestEnemies[0].transform.position, bulletPosition) <= BULLET_RANGE))
                {
                    closestEnemies[0].Hit(_currentDamage);

                    //:TODO: add an explosion sound

                    GameObject.Destroy(gameObject);
                }
                else
                {
                    transform.Translate(Vector3.forward * BULLET_SPEED * Time.deltaTime);
                }
            }
        }

        public void SetDamage(int damage)
        {
            _currentDamage = damage;
        }
    }
}