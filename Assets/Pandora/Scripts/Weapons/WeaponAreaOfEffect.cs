using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Pandora
{
    public class WeaponAreaOfEffect : MonoBehaviour
    {
        private static readonly float ATTACK_CHARGE_TIME = 0.8f;
        private static readonly float ATTACK_ENEMY_SIZE_OFFSET = 1.0f;

        [SerializeField] private Animator _attackAnimator;
        [SerializeField] private AudioSource _chargeSound;
        [SerializeField] private AudioSource _attackSound;
        [SerializeField] private Transform _weaponRange;
        [SerializeField] private ParticleSystem _weaponChargeParticles;

        private float _attackCooldowm;
        private float _attackRange;

        private float _currentAttackCooldown;

        private static readonly int ATTACK = Animator.StringToHash("Attack");
        private static readonly int BUILD_UP = Animator.StringToHash("BuildUp");

        private PlayerController _player;
        private bool _isCharging;

        private void Start()
        {
            _currentAttackCooldown = _attackCooldowm;

            _player = GetComponentInParent<PlayerController>();
        }

        private void Update()
        {
            if (_player.GetIsAlive())
            {
                if (_currentAttackCooldown > 0)
                {
                    _currentAttackCooldown = Mathf.Max(0, _currentAttackCooldown - Time.deltaTime);
                }

                if (!_isCharging && (_currentAttackCooldown <= ATTACK_CHARGE_TIME))
                {
                    _isCharging = true;
                    _attackAnimator.SetTrigger(BUILD_UP);
                    _chargeSound.Play();
                }

                if (_currentAttackCooldown <= 0)
                {
                    _attackSound.Play();
                    _attackAnimator.SetTrigger(ATTACK);
                    _weaponChargeParticles.Play();

                    _currentAttackCooldown = _attackCooldowm;
                    _isCharging = false;

                    EnemyController[] enemyControllers = GameObject.FindObjectsOfType<EnemyController>();
                    List<EnemyController> enemiesInRange = Utilities.GetEnemiesInRange(enemyControllers, transform.position, _attackRange);

                    int hitCount = 0;

                    foreach (EnemyController enemyController in enemiesInRange)
                    {
                        enemyController.Hit(1);
                        ++hitCount;
                    }

                    float shakeStrength = Mathf.Min(0.1f + 0.1f * hitCount, 3.0f);

                    _player.GetCamera().DOShakePosition(0.2f, shakeStrength, 15);
                }
            }
        }

        public void SetAttackCooldown(float attackCooldown)
        {
            _attackCooldowm = attackCooldown;
        }

        public void SetAttackRange(float attackRange)
        {
            _attackRange = attackRange + ATTACK_ENEMY_SIZE_OFFSET;
            _weaponRange.localScale = new Vector3(attackRange * 2.0f, 0.1f, attackRange * 2.0f);
        }
    }
}
