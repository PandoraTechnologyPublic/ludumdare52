using UnityEngine;

namespace Pandora
{
    public class PerkWeaponDirectionalMeleeDamage : PerkComponent
    {
        private static readonly float MIN_VALUE = 1.0f;
        private static readonly float MAX_VALUE = 4.0f;
        private static readonly int PERK_LEVEL_CAP = 3;

        public override string GetReference()
        {
            return "MELEE_DAMAGE";
        }

        public override int GetLevelCap()
        {
            return PERK_LEVEL_CAP;
        }

        public override void Upgrade(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            player.GetComponentInChildren<WeaponDirectionalMelee>().SetAttackDamage(GetValue(perkLevel));
        }

        public override string GetPerkName(PlayerController player)
        {
            return "Melee damage";
        }

        public override string GetPerkDescription(PlayerController player)
        {
            int walkSpeedLevel = player.GetPerkLevel(GetReference());

            return $"Improve melee damage from {GetValue(walkSpeedLevel)} to {GetValue(walkSpeedLevel + 1)}";
        }

        private static int GetValue(int level)
        {
            return Mathf.RoundToInt(MIN_VALUE + ((MAX_VALUE - MIN_VALUE) / PERK_LEVEL_CAP) * (level - 1));
        }
    }
}