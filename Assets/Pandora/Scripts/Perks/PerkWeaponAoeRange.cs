namespace Pandora
{
    public class PerkWeaponAoeRange : PerkComponent
    {
        private static readonly float MIN_ATTACK_RANGE = 3.0f;
        private static readonly float MAX_ATTACK_RANGE = 6.0f;
        private static readonly int PERK_LEVEL_CAP = 5;

        public override string GetReference()
        {
            return "AOE_ATTACK_RANGE";
        }

        public override int GetLevelCap()
        {
            return PERK_LEVEL_CAP;
        }

        public override void Upgrade(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            player.GetComponentInChildren<WeaponAreaOfEffect>().SetAttackRange(GetAttackRange(perkLevel));
        }

        public override string GetPerkName(PlayerController player)
        {
            return "Spell range";
        }

        public override string GetPerkDescription(PlayerController player)
        {
            int walkSpeedLevel = player.GetPerkLevel(GetReference());

            return $"Improve spell attack range from {GetAttackRange(walkSpeedLevel)} to {GetAttackRange(walkSpeedLevel + 1)}";
        }

        private static float GetAttackRange(int level)
        {
            return MIN_ATTACK_RANGE + ((MAX_ATTACK_RANGE - MIN_ATTACK_RANGE) / PERK_LEVEL_CAP) * (level -1);
        }
    }
}