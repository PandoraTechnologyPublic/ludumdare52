using UnityEngine;

namespace Pandora
{
    public abstract class PerkComponent : MonoBehaviour
    {
        // :TODO: introduce perk rarity

        public abstract void Upgrade(PlayerController player);

        public abstract string GetPerkName(PlayerController player);

        public abstract string GetPerkDescription(PlayerController player);

        public virtual string GetPerkLevelText(PlayerController player)
        {
            return $"Level {player.GetPerkLevel(GetReference())}";
        }

        public abstract string GetReference();

        public abstract int GetLevelCap();

        public void LevelUp(PlayerController player)
        {
            if (HasLevelTracking())
            {
                int walkSpeedLevel = player.GetPerkLevel(GetReference());

                player.SetPerkLevel(GetReference(), walkSpeedLevel + 1);
            }

            Upgrade(player);
        }

        public virtual bool HasLevelTracking()
        {
            return true;
        }

        public bool IsMaxed(PlayerController player)
        {
            bool isMaxed = false;

            if (HasLevelTracking())
            {
                int level = player.GetPerkLevel(GetReference());

                isMaxed = level > GetLevelCap();
            }

            return isMaxed;
        }
    }
}