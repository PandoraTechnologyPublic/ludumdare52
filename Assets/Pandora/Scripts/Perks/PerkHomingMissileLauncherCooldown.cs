namespace Pandora
{
    public class PerkWeaponHomingMissileLauncherCooldown : PerkComponent
    {
        private static readonly float MIN_ATTACK_COOLDOWN = 1.0f;
        private static readonly float MAX_ATTACK_COOLDOWN = 10.0f;
        private static readonly int PERK_LEVEL_CAP = 5;

        public override string GetReference()
        {
            return "MISSILE_LAUNCHER_COOLDOWN";
        }

        public override int GetLevelCap()
        {
            return PERK_LEVEL_CAP;
        }

        public override void Upgrade(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            player.GetComponentInChildren<WeaponHomingMissileLauncher>().SetAttackCooldown(GetAttackCooldown(perkLevel));
        }

        public override string GetPerkName(PlayerController player)
        {
            return "Homing missile cooldown";
        }

        public override string GetPerkDescription(PlayerController player)
        {
            int walkSpeedLevel = player.GetPerkLevel(GetReference());

            return $"Reduce homing missile launcher cooldown from {GetAttackCooldown(walkSpeedLevel)} to {GetAttackCooldown(walkSpeedLevel + 1)}";
        }

        private static float GetAttackCooldown(int level)
        {
            return MIN_ATTACK_COOLDOWN + ((MAX_ATTACK_COOLDOWN - MIN_ATTACK_COOLDOWN) / PERK_LEVEL_CAP) * (PERK_LEVEL_CAP - (level - 1));
        }
    }
}