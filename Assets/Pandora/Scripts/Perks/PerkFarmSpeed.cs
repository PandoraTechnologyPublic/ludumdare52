using UnityEngine;

namespace Pandora
{
    public class PerkFarmSpeed : PerkComponent
    {
        private static readonly float MIN_FARM_SPEED = 1.0f;
        private static readonly float MAX_FARM_SPEED = 4.0f;
        private static readonly int PERK_LEVEL_CAP = 10;

        public override string GetReference()
        {
            return "FARM_SPEED";
        }

        public override int GetLevelCap()
        {
            return PERK_LEVEL_CAP;
        }

        public override void Upgrade(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            player.SetFarmSpeed(GetFarmSpeed(perkLevel));
        }

        public override string GetPerkName(PlayerController player)
        {
            return "Farm speed";
        }

        public override string GetPerkDescription(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            return $"Improve farm speed from {GetFarmSpeed(perkLevel)} to {GetFarmSpeed(perkLevel + 1)}";
        }

        private static float GetFarmSpeed(int level)
        {
            return MIN_FARM_SPEED + ((MAX_FARM_SPEED - MIN_FARM_SPEED) / PERK_LEVEL_CAP) * (level - 1);
        }
    }
}