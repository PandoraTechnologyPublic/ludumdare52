using UnityEngine;

namespace Pandora
{
    public class PerkMaxLife : PerkComponent
    {
        private static readonly int MIN_MAX_LIFE = 10;
        private static readonly int MAX_MAX_LIFE = 160;
        private static readonly int PERK_LEVEL_CAP = 15;

        public override string GetReference()
        {
            return "MAX_LIFE";
        }

        public override int GetLevelCap()
        {
            return PERK_LEVEL_CAP;
        }

        public override void Upgrade(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            player.SetMaxLife(PerkMaxLife.GetMaxLife(perkLevel));
        }

        public override string GetPerkName(PlayerController player)
        {
            return "Max life";
        }

        public override string GetPerkDescription(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            return $"Increase max life from {GetMaxLife(perkLevel)} to {GetMaxLife(perkLevel + 1)}";
        }

        private static int GetMaxLife(int level)
        {
            return MIN_MAX_LIFE + ((MAX_MAX_LIFE - MIN_MAX_LIFE) / PERK_LEVEL_CAP) * (level -1);
        }
    }
}