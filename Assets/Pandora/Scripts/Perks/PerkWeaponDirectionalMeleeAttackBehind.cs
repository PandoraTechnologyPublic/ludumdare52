namespace Pandora
{
    public class PerkWeaponDirectionalMeleeAttackBehind : PerkComponent
    {
        private static readonly int PERK_LEVEL_CAP = 1;

        public override string GetReference()
        {
            return "DIRECTIONAL_MELEE_BEHIND";
        }

        public override int GetLevelCap()
        {
            return PERK_LEVEL_CAP;
        }

        public override void Upgrade(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            if (perkLevel > 1)
            {
                player.GetComponentInChildren<WeaponDirectionalMelee>().SetAttackingBehind();
            }
        }

        public override string GetPerkName(PlayerController player)
        {
            return "Melee attack behind";
        }

        public override string GetPerkDescription(PlayerController player)
        {
            return $"Improve melee attack to hit enemies behind";
        }
    }
}