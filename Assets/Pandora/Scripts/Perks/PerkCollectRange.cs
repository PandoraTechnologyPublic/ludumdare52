using UnityEngine;

namespace Pandora
{
    public class PerkCollectRange : PerkComponent
    {
        private static readonly float MIN_COLLECTION_RANGE = 2.0f;
        private static readonly float MAX_COLLECTION_RANGE = 5.0f;
        private static readonly int PERK_LEVEL_CAP = 10;

        public override string GetReference()
        {
            return "COLLECT_RANGE";
        }

        public override int GetLevelCap()
        {
            return PERK_LEVEL_CAP;
        }

        public override void Upgrade(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            player.SetCollectionRange(GetCollectionRange(perkLevel));
        }

        public override string GetPerkName(PlayerController player)
        {
            return "Collection range";
        }

        public override string GetPerkDescription(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            return $"Improve seeds collection range from {GetCollectionRange(perkLevel)} to {GetCollectionRange(perkLevel + 1)}";
        }

        private static float GetCollectionRange(int level)
        {
            return MIN_COLLECTION_RANGE + ((MAX_COLLECTION_RANGE - MIN_COLLECTION_RANGE) / PERK_LEVEL_CAP) * (level - 1);
        }
    }
}