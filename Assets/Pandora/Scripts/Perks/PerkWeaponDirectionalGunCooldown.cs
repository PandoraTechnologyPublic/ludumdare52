namespace Pandora
{
    public class PerkWeaponDirectionalGunCooldown : PerkComponent
    {
        private static readonly float MIN_ATTACK_COOLDOWN = 0.3f;
        private static readonly float MAX_ATTACK_COOLDOWN = 2.0f;
        private static readonly int PERK_LEVEL_CAP = 5;

        public override string GetReference()
        {
            return "DIRECTIONAL_GUN_COOLDOWN";
        }

        public override int GetLevelCap()
        {
            return PERK_LEVEL_CAP;
        }

        public override void Upgrade(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            player.GetComponentInChildren<WeaponDirectionalGun>().SetAttackCooldown(GetAttackCooldown(perkLevel));
        }

        public override string GetPerkName(PlayerController player)
        {
            return "Gun cooldown";
        }

        public override string GetPerkDescription(PlayerController player)
        {
            int walkSpeedLevel = player.GetPerkLevel(GetReference());

            return $"Improve gun cooldown from {GetAttackCooldown(walkSpeedLevel)} to {GetAttackCooldown(walkSpeedLevel + 1)}";
        }

        private static float GetAttackCooldown(int level)
        {
            return MIN_ATTACK_COOLDOWN + ((MAX_ATTACK_COOLDOWN - MIN_ATTACK_COOLDOWN) / PERK_LEVEL_CAP) * (PERK_LEVEL_CAP - (level - 1));
        }
    }
}