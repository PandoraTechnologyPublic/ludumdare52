using UnityEngine;

namespace Pandora
{
    public class PerkHomingMissileLauncherDamage : PerkComponent
    {
        private static readonly float MIN_VALUE = 1;
        private static readonly float MAX_VALUE = 11;
        private static readonly int PERK_LEVEL_CAP = 10;

        public override string GetReference()
        {
            return "MISSILE_LAUNCHER_DAMAGE";
        }

        public override int GetLevelCap()
        {
            return PERK_LEVEL_CAP;
        }

        public override void Upgrade(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            player.GetComponentInChildren<WeaponHomingMissileLauncher>().SetAttackDamage(GetValue(perkLevel));
        }

        public override string GetPerkName(PlayerController player)
        {
            return "Homing missile damage";
        }

        public override string GetPerkDescription(PlayerController player)
        {
            int walkSpeedLevel = player.GetPerkLevel(GetReference());

            return $"Improve homing missile damage from {GetValue(walkSpeedLevel)} to {GetValue(walkSpeedLevel + 1)}";
        }

        private static int GetValue(int level)
        {
            return Mathf.RoundToInt(MIN_VALUE + ((MAX_VALUE - MIN_VALUE) / PERK_LEVEL_CAP) * (level - 1));
        }
    }
}