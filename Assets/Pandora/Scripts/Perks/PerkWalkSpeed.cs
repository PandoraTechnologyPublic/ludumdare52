namespace Pandora
{
    public class PerkWalkSpeed : PerkComponent
    {
        private static readonly float MIN_WALK_SPEED = 6.0f;
        private static readonly float MAX_WALK_SPEED = 10.0f;
        private static readonly int PERK_LEVEL_CAP = 10;

        public override string GetReference()
        {
            return "WALK_SPEED";
        }

        public override int GetLevelCap()
        {
            return PERK_LEVEL_CAP;
        }

        public override void Upgrade(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            player.SetWalkSpeed(PerkWalkSpeed.GetWalkSpeed(perkLevel));
        }

        public override string GetPerkName(PlayerController player)
        {
            return "Walk speed";
        }

        public override string GetPerkDescription(PlayerController player)
        {
            int walkSpeedLevel = player.GetPerkLevel(GetReference());

            return $"Increase walk speed from {GetWalkSpeed(walkSpeedLevel)} to {GetWalkSpeed(walkSpeedLevel + 1)}";
        }

        private static float GetWalkSpeed(int level)
        {
            return MIN_WALK_SPEED + ((MAX_WALK_SPEED - MIN_WALK_SPEED) / PERK_LEVEL_CAP) * (level -1);
        }
    }
}