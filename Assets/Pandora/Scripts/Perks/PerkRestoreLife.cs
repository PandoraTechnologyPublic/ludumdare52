using UnityEngine;

namespace Pandora
{
    public class PerkRestoreLife : PerkComponent
    {
        private static readonly int RESTORED_LIFE_POINT_COUNT = 5;

        public override string GetPerkLevelText(PlayerController player)
        {
            return "";
        }

        public override string GetReference()
        {
            return "MAX_LIFE";
        }

        public override int GetLevelCap()
        {
            return 0;
        }

        public override void Upgrade(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            player.RestoreLife(RESTORED_LIFE_POINT_COUNT);
        }

        public override string GetPerkName(PlayerController player)
        {
            return "Restore life";
        }

        public override string GetPerkDescription(PlayerController player)
        {
            int perkLevel = player.GetPerkLevel(GetReference());

            return $"Restore up to {RESTORED_LIFE_POINT_COUNT} life point";
        }

        public override bool HasLevelTracking()
        {
            return false;
        }
    }
}