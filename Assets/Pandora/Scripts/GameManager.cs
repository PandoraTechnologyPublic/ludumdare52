using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

namespace Pandora
{
    public class GameManager : MonoBehaviour
    {

        private static readonly float FIELD_SPAWN_COOLDOWN = 10.0f;
        private static readonly int MAX_FIELD_SPAWNED_COUNT = 5;
        private static readonly float FIELD_SPAWN_ZONE_RANGE = 40.0f;
        private static readonly float MIN_FIELD_DISTANCE_RANGE = 20.0f;
        private static readonly int FIELD_TO_FARM_COUNT = 15;

        private static readonly float INITIAL_ENEMY_SPAWN_COOLDOWN = 3.0f;
        private static readonly float ENEMY_SPAWN_RANGE = 30.0f;
        private static readonly float ENEMY_SPAWN_SPEED_INCREASE_COOLDOWN = 15.0f;
        private static readonly float INITIAL_ENEMY_SPAWN_COOLDOWN_INCREASE_PERCENTAGE = 0.1f;
        private static readonly float ENEMY_MAX_SPAWN_COOLDOWN = 1.0f;

        private static readonly float INITIAL_DROP_RATE_PERCENTAGE = 0.8f;

        private static readonly int MIN_GENERATED_SEED_FROM_FIELD = 5;
        private static readonly int MAX_GENERATED_SEED_FROM_FIELD = 10;

        [SerializeField] private List<EnemyController> _enemyPrototypes;
        [SerializeField] private List<DropComponent> _dropPrototypes;
        [SerializeField] private FieldComponent _fieldPrefab;
        [SerializeField] private List<FieldComponent> _fields;

        private float _currentEnemySpawnCooldownValue;
        private float _currentEnemySpawnCooldown;
        private float _currentEnemySpawnSpeedIncreaseCooldown;

        private float _currentFieldSpawnCooldown;
        // private readonly List<FieldComponent> _fields = new List<FieldComponent>();

        private bool _isGameOver = false;

        private int _enemyKilledCount;
        private int _farmedFieldCount;

        private readonly List<DropComponent> _drops = new List<DropComponent>();

        private InputManager _inputManager;
        private GuiManager _guiManager;
        private PlayerController _player;
        private float _elapsedTime;
        private int _pauseCount;

        private void Start()
        {
            _guiManager = GameObject.FindObjectOfType<GuiManager>();
            _inputManager = GameObject.FindObjectOfType<InputManager>();
            _player = GameObject.FindObjectOfType<PlayerController>();

            _currentEnemySpawnCooldownValue = INITIAL_ENEMY_SPAWN_COOLDOWN;
            _currentEnemySpawnCooldown = _currentEnemySpawnCooldownValue;
            _currentEnemySpawnSpeedIncreaseCooldown = ENEMY_SPAWN_SPEED_INCREASE_COOLDOWN;

            _currentFieldSpawnCooldown = FIELD_SPAWN_COOLDOWN;

            _guiManager.SetFieldFarmed(_farmedFieldCount, FIELD_TO_FARM_COUNT);
        }

        private void Update()
        {
            if (_isGameOver)
            {
                if (_inputManager.GetGameInputs().UI.Restart.WasReleasedThisFrame())
                {
                    SceneManager.LoadScene(1);
                }
            }
            else
            {
                _elapsedTime += Time.deltaTime;

                int fieldToFarmLeft = FIELD_TO_FARM_COUNT - _farmedFieldCount;
                int neededField = Mathf.Min(MAX_FIELD_SPAWNED_COUNT, fieldToFarmLeft);

                if (_fields.Count < neededField)
                {
                    if (_currentFieldSpawnCooldown > 0)
                    {
                        _currentFieldSpawnCooldown = Mathf.Max(0, _currentFieldSpawnCooldown - Time.deltaTime);
                    }

                    if (_currentFieldSpawnCooldown <= 0)
                    {
                        Vector3 fieldSpawnPosition = new Vector3(Random.Range(-FIELD_SPAWN_ZONE_RANGE, FIELD_SPAWN_ZONE_RANGE), 0, Random.Range(-FIELD_SPAWN_ZONE_RANGE, FIELD_SPAWN_ZONE_RANGE));//playerPosition + (Quaternion.AngleAxis(Random.Range(0, 360.0f), Vector3.up) * Vector3.forward * FIELD_SPAWN_RANGE));
                        bool isFieldTooClose = Vector3.Distance(fieldSpawnPosition, _player.transform.position) <= MIN_FIELD_DISTANCE_RANGE;

                        foreach (FieldComponent fieldComponent in _fields)
                        {
                            if (Vector3.Distance(fieldSpawnPosition, fieldComponent.transform.position) <= MIN_FIELD_DISTANCE_RANGE)
                            {
                                isFieldTooClose = true;

                                break;
                            }
                        }

                        if (!isFieldTooClose && Physics.Raycast(fieldSpawnPosition + Vector3.up, Vector3.down))
                        {
                            FieldComponent fieldComponent = GameObject.Instantiate(_fieldPrefab, fieldSpawnPosition, Quaternion.identity);

                            _fields.Add(fieldComponent);

                            _currentFieldSpawnCooldown = FIELD_SPAWN_COOLDOWN;
                        }
                    }
                }

                if (_currentEnemySpawnCooldown > 0)
                {
                    _currentEnemySpawnCooldown = Mathf.Max(0, _currentEnemySpawnCooldown - Time.deltaTime);
                }

                if (_currentEnemySpawnCooldown <= 0)
                {
                    EnemyController enemyController = _enemyPrototypes[Random.Range(0, _enemyPrototypes.Count)];

                    if (_elapsedTime > enemyController.GetAppearanceThreshold())
                    {
                        Vector3 playerPosition = _player.transform.position;
                        Vector3 enemySpawnPosition = playerPosition + (Quaternion.AngleAxis(Random.Range(0, 360.0f), Vector3.up) * Vector3.forward * ENEMY_SPAWN_RANGE);

                        if (Physics.Raycast(enemySpawnPosition+ Vector3.up, Vector3.down))
                        {
                            GameObject.Instantiate(enemyController, enemySpawnPosition, Quaternion.identity);

                            _currentEnemySpawnCooldown = _currentEnemySpawnCooldownValue;
                        }
                    }
                }

                if (_currentEnemySpawnSpeedIncreaseCooldown > 0)
                {
                    _currentEnemySpawnSpeedIncreaseCooldown = Mathf.Max(0, _currentEnemySpawnSpeedIncreaseCooldown - Time.deltaTime);
                }

                if (_currentEnemySpawnSpeedIncreaseCooldown <= 0)
                {
                    _currentEnemySpawnCooldownValue = Mathf.Max(ENEMY_MAX_SPAWN_COOLDOWN, _currentEnemySpawnCooldownValue - INITIAL_ENEMY_SPAWN_COOLDOWN_INCREASE_PERCENTAGE);
                    _currentEnemySpawnSpeedIncreaseCooldown = ENEMY_SPAWN_SPEED_INCREASE_COOLDOWN;
                }
            }
        }

        public void GameOver()
        {
            _guiManager.ShowGameOverScreen();
            _isGameOver = true;
        }

        public void AddEnemyKilled(Vector3 enemyPosition)
        {
            _enemyKilledCount += 1;

            if (Random.Range(0, 1.0f) < INITIAL_DROP_RATE_PERCENTAGE)
            {
                DropComponent dropComponent = GameObject.Instantiate(_dropPrototypes[Random.Range(0, _dropPrototypes.Count)], enemyPosition, Quaternion.identity);

                _drops.Add(dropComponent);

                Vector3 positionTarget = enemyPosition + new Vector3(Random.Range(-3, 3), 0, Random.Range(-3, 3));

                dropComponent.transform.DOJump(positionTarget, 3.0f, 1, 0.4f);
            }
        }

        public void AddFarmedField(FieldComponent field)
        {
            _farmedFieldCount += 1;

            int generatedDropCount = Random.Range(MIN_GENERATED_SEED_FROM_FIELD, MAX_GENERATED_SEED_FROM_FIELD);
            Vector3 fieldPosition = field.transform.position;

            for (int seedIndex = 0; seedIndex < generatedDropCount; seedIndex++)
            {
                DropComponent dropComponent = GameObject.Instantiate(_dropPrototypes[Random.Range(0, _dropPrototypes.Count)], fieldPosition, Quaternion.identity);

                _drops.Add(dropComponent);

                Vector3 positionTarget = fieldPosition + new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));

                dropComponent.transform.DOJump(positionTarget, 5.0f, 1, 0.4f);
            }

            _fields.Remove(field);

            GameObject.Destroy(field.gameObject);

            _guiManager.SetFieldFarmed(_farmedFieldCount, FIELD_TO_FARM_COUNT);

            if (_farmedFieldCount >= FIELD_TO_FARM_COUNT)
            {
                _guiManager.ShowVictoryScreen();
            }
        }

        public List<DropComponent> GetDrops()
        {
            return _drops;
        }

        public float GetElapsedTime()
        {
            return _elapsedTime;
        }

        public void PauseGame()
        {
            ++_pauseCount;

            if (_pauseCount > 0)
            {
                Time.timeScale = 0;
            }
        }

        public void UnPauseGame()
        {
            --_pauseCount;

            if (_pauseCount <= 0)
            {
                Time.timeScale = 1;
                _pauseCount = 0;
            }
        }
    }
}