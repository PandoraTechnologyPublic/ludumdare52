using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;

namespace Pandora
{
    public class EnemyController : MonoBehaviour
    {
        private static readonly float INITIAL_ENEMY_REACH = 2.0f;
        private static readonly float INITIAL_ENEMY_DAMAGE = 1.0f;
        private static readonly float INITIAL_ENEMY_ATTACK_COOLDOWN = 0.5f;

        [SerializeField] private int _initialSpeed = 4;
        [SerializeField] private int _initialLifePoints = 1;
        [SerializeField] private float _enemyAppearanceThreshold;

        [SerializeField] private List<SpriteRenderer> _spriteRenderers;
        [SerializeField] private AudioSource _deathSound;
        [SerializeField] private AudioSource _hitSound;
        [SerializeField] private Animator _animator;
        [Tooltip("Should we play the anim backwards when moving towards +z ?")]
        [SerializeField] private bool _animBackwards;

        private float _speed;
        private float _lifePoints;
        private float _enemyReach;
        private float _enemyDamage;
        private float _attackCooldowm;
        
        private float _currentAttackCooldown;

        private Vector3 _playerDirection;

        private PlayerController _player;
        private NavMeshAgent _navMeshAgent;

        private GameManager _gameManager;
        private GuiManager _guiManager;
        private static readonly int MOVING = Animator.StringToHash("moving");
        private static readonly int MULT = Animator.StringToHash("mult");

        private void Start()
        {
            _gameManager = GameObject.FindObjectOfType<GameManager>();
            _guiManager = GameObject.FindObjectOfType<GuiManager>();

            _speed = _initialSpeed;
            _enemyReach = INITIAL_ENEMY_REACH;
            _enemyDamage = INITIAL_ENEMY_DAMAGE;
            _attackCooldowm = INITIAL_ENEMY_ATTACK_COOLDOWN;
            _lifePoints = _initialLifePoints;

            _player = GameObject.FindObjectOfType<PlayerController>();
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        private void Update()
        {
            _playerDirection = _player.transform.position - transform.position;

            if (_currentAttackCooldown > 0)
            {
                _currentAttackCooldown = Mathf.Max(0, _currentAttackCooldown - Time.deltaTime);
            }

            Vector3 moveValues = Vector3.zero;

            if (_player.GetIsAlive())
            {
                if (_lifePoints > 0)
                {
                    if (_playerDirection.magnitude < _enemyReach)
                    {
                        if (_currentAttackCooldown <= 0)
                        {
                            _player.Hit(_enemyDamage);
                            _currentAttackCooldown = _attackCooldowm;
                        }
                    }
                    else
                    {
                        moveValues = _playerDirection.normalized * Time.deltaTime * _speed;
                        moveValues.y = 0;

                        foreach (SpriteRenderer spriteRenderer in _spriteRenderers)
                        {
                            spriteRenderer.flipX = (moveValues.x <= 0);
                        }

                        _navMeshAgent.Move(moveValues);
                    }
                }
            }

            _animator.SetBool(MOVING, !moveValues.Equals(Vector3.zero));

            if (_animBackwards)
            {
                _animator.SetFloat(MULT, moveValues.z > 0 ? -1 : 1);

                foreach (SpriteRenderer spriteRenderer in _spriteRenderers)
                {
                    spriteRenderer.flipX = (moveValues.x <= 0 != moveValues.z > 0);
                }
            }
        }

        public float GetAppearanceThreshold()
        {
            return _enemyAppearanceThreshold;
        }

        public void Hit(int damageCount)
        {
            if (_lifePoints > 0)
            {
                _lifePoints = Mathf.Max(_lifePoints - damageCount, 0);

                _hitSound.Play();

                Sequence hitSequence = DOTween.Sequence();

                foreach (SpriteRenderer spriteRenderer in _spriteRenderers)
                {
                    hitSequence.Append(spriteRenderer.material.DOColor(new Color(10f, 10f, 10f, 2f), 0f));
                }

                hitSequence.AppendInterval(0.1f);

                foreach (SpriteRenderer spriteRenderer in _spriteRenderers)
                {
                    hitSequence.Join(spriteRenderer.material.DOColor(new Color(10f, 10f, 10f, 2f), 0.1f));
                }

                foreach (SpriteRenderer spriteRenderer in _spriteRenderers)
                {
                    hitSequence.Append(spriteRenderer.material.DOColor(Color.white, 0f));
                }

                _guiManager.AnimateHit(transform.position + Vector3.up * 2, damageCount);

                if (_lifePoints <= 0)
                {

                    _deathSound.Play();

                    Sequence deathSequence = DOTween.Sequence();

                    // foreach (SpriteRenderer spriteRenderer in _spriteRenderers)
                    {
                        deathSequence.Join(_spriteRenderers[0].transform.DOBlendableMoveBy(-_playerDirection, 0.2f));
                    }

                    foreach (SpriteRenderer spriteRenderer in _spriteRenderers)
                    {
                        deathSequence.Join(spriteRenderer.material.DOColor(Color.black, _deathSound.clip.length));
                    }

                    ;

                    deathSequence.AppendCallback(() => _gameManager.AddEnemyKilled(transform.position - _playerDirection));
                    deathSequence.AppendCallback(() => GameObject.Destroy(gameObject));
                }
            }
        }
    }
}