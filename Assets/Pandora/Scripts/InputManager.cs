using System;
using UnityEngine;

namespace Pandora
{
    public class InputManager : MonoBehaviour
    {
        private Inputs _gameInputs;

        private void Start()
        {
            _gameInputs = new Inputs();
            _gameInputs.Enable();
        }

        private void OnDisable()
        {
            _gameInputs.Disable();
        }

        public Inputs GetGameInputs()
        {
            return _gameInputs;
        }
    }
}