using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;

namespace Pandora
{
    public class PlayerController : MonoBehaviour
    {
        private static readonly float INITIAL_PLAYER_LIFE_POINTS = 10.0f;
        private static readonly int INITIAL_PLAYER_LEVEL_UP_COUNT = 5;
        private static readonly int LEVEL_UP_XP_INCREASE_COUNT = 1;
        private static readonly int FIELD_INTERACTION_DISTANCE = 4;
        private static readonly float PLAYER_HIT_COOLDOWN = 0.5f;

        [SerializeField] private AudioSource _hitSound;
        [SerializeField] private AudioSource _seedCollectedSound;
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private Animator _animator;

        [SerializeField] private List<PerkComponent> _perks;

        private float _currentLifePoints;
        private float _maxLifePoints;
        private float _playerSpeed;
        private int _dropCount;
        private int _currentXpNeededToLevelUpCount;
        private float _collectionRange;
        private float _farmSpeed;
        private float _currentPlayerHitCooldown;

        private readonly Dictionary<string, int> _perkLevels = new Dictionary<string, int>();

        private Camera _camera;

        private InputManager _inputManager;
        private GuiManager _guiManager;
        private GameManager _gameManager;
        private NavMeshAgent _navMeshAgent;
        private Vector3 _lastPlayerDirection = Vector3.forward;

        private void Start()
        {
            _gameManager = GameObject.FindObjectOfType<GameManager>();
            _guiManager = GameObject.FindObjectOfType<GuiManager>();
            _inputManager = GameObject.FindObjectOfType<InputManager>();

            _navMeshAgent = GetComponent<NavMeshAgent>();

            _camera = GetComponentInChildren<Camera>();

            _currentLifePoints = INITIAL_PLAYER_LIFE_POINTS;
            _maxLifePoints = INITIAL_PLAYER_LIFE_POINTS;

            _currentXpNeededToLevelUpCount = INITIAL_PLAYER_LEVEL_UP_COUNT;

            _guiManager.SetPlayerLifePoints(_currentLifePoints, _maxLifePoints);
            _guiManager.SetPlayerXpPoints(_dropCount, _currentXpNeededToLevelUpCount);

            foreach (PerkComponent perkComponent in _perks)
            {
                if (perkComponent.HasLevelTracking())
                {
                    _perkLevels.Add(perkComponent.GetReference(), 1);
                    perkComponent.Upgrade(this);
                }
            }
        }

        // private void FixedUpdate()
        // {
        //     if (_currentLifePoints > 0)
        //     {
        //         Vector2 moveValues = _inputManager.GetGameInputs().Player.Move.ReadValue<Vector2>();
        //
        //         moveValues.Normalize();
        //         moveValues *= Time.fixedDeltaTime * _playerSpeed;
        //
        //         Vector3 playerDirection = new Vector3(moveValues.x, 0, moveValues.y);
        //
        //         if (!moveValues.Equals(Vector2.zero))
        //         {
        //             _spriteRenderer.flipX = moveValues.x < 0;
        //             _lastPlayerDirection = playerDirection;
        //         }
        //
        //          _rigidbody.MovePosition(transform.position + playerDirection);
        //         _animator.SetBool("moving", !moveValues.Equals(Vector2.zero));
        //     }
        // }

        private void Update()
        {
            if (_currentLifePoints > 0)
            {
                Vector2 moveValues = _inputManager.GetGameInputs().Player.Move.ReadValue<Vector2>();

                moveValues.Normalize();
                moveValues *= Time.deltaTime * _playerSpeed;

                Vector3 playerDirection = new Vector3(moveValues.x, 0, moveValues.y);

                if (!moveValues.Equals(Vector2.zero))
                {
                    if (moveValues.x != 0)
                    {
                        _spriteRenderer.flipX = moveValues.x < 0;
                    }

                    _lastPlayerDirection = playerDirection;
                }

                _navMeshAgent.Move(playerDirection);
                //_rigidbody.MovePosition(transform.position + playerDirection);
                _animator.SetBool("moving", !moveValues.Equals(Vector2.zero));
            }

            if (_currentPlayerHitCooldown > 0)
            {
                _currentPlayerHitCooldown = Mathf.Max(0, _currentPlayerHitCooldown - Time.deltaTime);
            }

            List<DropComponent> dropComponents = _gameManager.GetDrops();

            for (int dropIndex = dropComponents.Count - 1; dropIndex >= 0; dropIndex--)
            {
                DropComponent dropComponent = dropComponents[dropIndex];

                if (dropComponent.IsPickable() && Vector3.Distance(dropComponent.transform.position, transform.position) <= _collectionRange)
                {
                    ++_dropCount;
                    dropComponents.RemoveAt(dropIndex);
                    GameObject.Destroy(dropComponent.gameObject);
                    _seedCollectedSound.Play();
                    _guiManager.SetPlayerXpPoints(_dropCount, _currentXpNeededToLevelUpCount);

                    if (_dropCount >= _currentXpNeededToLevelUpCount)
                    {
                        _guiManager.ShowLevelUpScreen();
                        _dropCount -= _currentXpNeededToLevelUpCount;
                        _guiManager.SetPlayerXpPoints(_dropCount, _currentXpNeededToLevelUpCount);

                        _currentXpNeededToLevelUpCount += LEVEL_UP_XP_INCREASE_COUNT;
                    }
                }
            }

            FieldComponent[] fields = GameObject.FindObjectsOfType<FieldComponent>();
            FieldComponent field = Utilities.GetClosestField(fields, transform.position);

            if ((field != null) && Vector3.Distance(field.transform.position, transform.position) <= FIELD_INTERACTION_DISTANCE)
            {
                field.Use(this);
            }
        }

        public bool GetIsAlive()
        {
            return _currentLifePoints > 0;
        }

        public void Hit(float damageCount)
        {
            if ((_currentLifePoints > 0) && (_currentPlayerHitCooldown <= 0))
            {
                _currentLifePoints = Mathf.Max(_currentLifePoints - damageCount, 0);

                _hitSound.Play();
                _guiManager.SetPlayerLifePoints(_currentLifePoints, _maxLifePoints);
                _guiManager.AnimateHit(transform.position + Vector3.up * 2, damageCount);

                _camera.DOShakePosition(0.3f, 0.3f);

                DOTween.Sequence()
                    .Append(_spriteRenderer.material.DOColor(new Color(4f, 2f, 3f, 2f), 0f))
                    .Append(_spriteRenderer.material.DOColor(new Color(4f, 2f, 3f, 2f), 0.1f))
                    .Append(_spriteRenderer.material.DOColor(Color.white, 0f));

                _currentPlayerHitCooldown = PLAYER_HIT_COOLDOWN;

                if (_currentLifePoints <= 0)
                {
                    _gameManager.GameOver();
                }
            }
        }

        public int GetPerkLevel(string perkName)
        {
            return _perkLevels[perkName];
        }

        public void SetPerkLevel(string perkName, int level)
        {
            _perkLevels[perkName] = level;
        }

        public List<PerkComponent> GetAvailablePerks()
        {
            List<PerkComponent> availablePerks = new List<PerkComponent>();

            foreach (PerkComponent perkComponent in _perks)
            {
                if (!perkComponent.IsMaxed(this))
                {
                    availablePerks.Add(perkComponent);
                }
            }

            return availablePerks;
        }

        public void SetWalkSpeed(float walkSpeed)
        {
            _playerSpeed = walkSpeed;
        }

        public void SetMaxLife(int maxLife)
        {
            _maxLifePoints = maxLife;
            _guiManager.SetPlayerLifePoints(_currentLifePoints, _maxLifePoints);
        }

        public void RestoreLife(int restoredLifePointCount)
        {
            _currentLifePoints = Mathf.Min(_currentLifePoints + restoredLifePointCount, _maxLifePoints);
            _guiManager.SetPlayerLifePoints(_currentLifePoints, _maxLifePoints);
        }

        public void SetCollectionRange(float collectionRange)
        {
            _collectionRange = collectionRange;
        }

        public Vector3 GetLastDirection()
        {
            return _lastPlayerDirection;
        }

        public bool GetIsLeftOriented()
        {
            return _spriteRenderer.flipX;
        }

        public void SetFarmSpeed(float farmSpeed)
        {
            _farmSpeed = farmSpeed;
        }

        public float GetFarmSpeed()
        {
            return _farmSpeed;
        }

        public Camera GetCamera()
        {
            return _camera;
        }
    }
}
